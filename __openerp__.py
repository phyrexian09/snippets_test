# -*- encoding: utf-8 -*-
#                                                                            #
#   OpenERP Module                                                           #
#   Copyright (C) 2015 Author pierre@adigit.com                              #
#                                                                            #
#   This program is free software: you can redistribute it and/or modify     #
#   it under the terms of the GNU Affero General Public License as           #
#   published by the Free Software Foundation, either version 3 of the       #
#   License, or (at your option) any later version.                          #
#                                                                            #
#   This program is distributed in the hope that it will be useful,          #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
#   GNU Affero General Public License for more details.                      #
#                                                                            #
#   You should have received a copy of the GNU Affero General Public License #
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                            #

{
    "name": "snippets_test", # Nom de votre snippet.

    "description": """ Ceci est un tuto """, # Description.

    "version": "1.0", # Version de votre module.

    "author": "Adigit", # Auteur.

    "website": "http://adigit.com", # Site Web.

    "category": "Theme", # Vous pouvez mettre la catégorie que vous désirez.

    "depends": ['website'], # Indispensable, vous déclarez les dépendances dont à besoin votre module pour fonctionner.

    'data': [  # Dans data, vous indiquez le chemin de toutes vos vues.
        'views/snippets.xml',
        'views/assets.xml', #Attention à ne pas déclarer un page XML vide, vous aurez une jolie erreur à l'installation.
        'views/options.xml',
    ],

    'installable': True, # True, afin d'installer l'application.
}
